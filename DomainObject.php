<?php

/**
 * Created by PhpStorm.
 * User: hillel
 * Date: 02.05.18
 * Time: 20:24
 */
abstract class DomainObject
{
    protected $group;

    public function __construct(){
        $this->group = static::getGroup();
    }

    static public function create(){
        return new static();
    }

    static public function getGroup(){
        return 'default';
    }
}